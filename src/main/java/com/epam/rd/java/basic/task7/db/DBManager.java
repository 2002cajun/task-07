package com.epam.rd.java.basic.task7.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static String URL;


	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			FileReader reader = new FileReader("app.properties");
			Properties p = new Properties();
			p.load(reader);
			URL = (String) p.get("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM users u");) {

			while (rs.next()) {
				User user = new User();
				user.setLogin(rs.getString("login"));
				user.setId(rs.getInt("id"));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {

			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			stmt = con.prepareStatement("INSERT INTO users (login) VALUES (?)",stmt.RETURN_GENERATED_KEYS);

			stmt.setString(1, user.getLogin());

			int count = stmt.executeUpdate();

			if(count > 0){
				try (ResultSet rs = stmt.getGeneratedKeys()){
					if(rs.next())
						user.setId(rs.getInt(1));
				}
			}
			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		int count = 0;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			for (User user : users) {


				stmt = con.prepareStatement("DELETE FROM users  WHERE login = (?)");


				stmt.setString(1, user.getLogin());

				count = stmt.executeUpdate();
			}

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		if(count > 0){
			return true;
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			stmt = con.prepareStatement("SELECT * FROM users u WHERE u.login = ?");

			stmt.setString(1,login);


			try (ResultSet rs = stmt.executeQuery();) {
				rs.next();
				user.setLogin(rs.getString("login"));
				user.setId(rs.getInt("id"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("", e);
		} finally {
			close(stmt);
			close(con);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			stmt = con.prepareStatement("SELECT * FROM teams t WHERE t.name = ?");
			stmt.setString(1,name);



			try (ResultSet rs = stmt.executeQuery();) {
				rs.next();
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("id"));

			}
			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("", e);
		} finally {
			close(stmt);
			close(con);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM teams t");) {
			 con.setAutoCommit(false);
			 con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			while (rs.next()) {
				Team team = new Team();
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			stmt = con.prepareStatement("INSERT INTO teams (name) VALUES (?)",PreparedStatement.RETURN_GENERATED_KEYS);

			stmt.setString(1, team.getName());

			int count = stmt.executeUpdate();

			if(count > 0){
				try (ResultSet rs = stmt.getGeneratedKeys()){
					if(rs.next())
						team.setId(rs.getInt(1));
				}
			}
			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

//            addUsersTeams(con,user);

			for (Team team:teams) {

				addTeamForUser(con, user.getId(), team);
			}

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		return true;

	}


	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			stmt = con.prepareStatement("SELECT team_id FROM users_teams u WHERE u.user_id=?");

			if (user.getId() == 0) {
				stmt.setNull(1, Types.INTEGER);
			} else {
				stmt.setInt(1, user.getId());
			}

			try (ResultSet rs = stmt.executeQuery();) {

				while (rs.next()) {
//					Team team = new Team();
//					team.setName(String.valueOf( rs.getInt("team_id")));
//					teams.add(team);
					teams.add(findTeam(rs.getInt("team_id")));
				}
			}
			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(stmt);
			close(con);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		int count = 0;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			stmt = con.prepareStatement("DELETE FROM teams  WHERE name = (?)");


			stmt.setString(1, team.getName());

			count = stmt.executeUpdate();

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		if(count > 0){
			return true;
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		int count = 0;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			stmt = con.prepareStatement("UPDATE teams SET name = ?  WHERE id = (?)");


			stmt.setString(1, team.getName());
			stmt.setInt(2,team.getId());

			count = stmt.executeUpdate();

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(con);
			close(stmt);
		}
		if(count > 0){
			return true;
		}
		return false;

	}

	private void close(AutoCloseable autoCloseable) {
		try {
			if(autoCloseable!=null)
				autoCloseable.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void rollback(Connection con) {
		try {
			con.rollback();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	private void addUsersTeams(Connection con, User user)throws SQLException {
		try (PreparedStatement  stmt = con.prepareStatement("INSERT INTO users_teams (user_id) VALUES (?)",
				Statement.RETURN_GENERATED_KEYS);) {

			if (user.getId() == 0) {
				stmt.setNull(1, Types.INTEGER);

			} else {
				stmt.setInt(1, user.getId());
			}



			int affectedRows = stmt.executeUpdate();
			// check the affected rows
			if (affectedRows > 0) {
				// get the ID back
				try (ResultSet rs = stmt.getGeneratedKeys();) {
					if (rs.next()) {
						user.setId(rs.getInt(1));
					}
				}
			}
		}
	}

	private void addTeamForUser(Connection con, int userId , Team team) throws SQLException{
		try (PreparedStatement  stmt = con.prepareStatement("INSERT INTO users_teams (user_id,team_id) VALUES (?,?)",
				Statement.RETURN_GENERATED_KEYS);) {

			if (userId == 0) {
				stmt.setNull(1, Types.INTEGER);

			} else {
				stmt.setInt(1, userId);
			}

			if (team.getId() == 0) {
				stmt.setNull(2, Types.INTEGER);

			} else {
				stmt.setInt(2, team.getId());
			}

			stmt.executeUpdate();

		}

	}

	private Team findTeam(int teamId) throws DBException {
		Team team = new Team();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			stmt = con.prepareStatement("SELECT * FROM teams t WHERE t.id=?");

			if (teamId == 0) {
				stmt.setNull(1, Types.INTEGER);
			} else {
				stmt.setInt(1, teamId);
			}

			try (ResultSet rs = stmt.executeQuery();) {
				rs.next();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));

			}
			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("", e);
		} finally {
			close(stmt);
			close(con);
		}
		return team;
	}

}

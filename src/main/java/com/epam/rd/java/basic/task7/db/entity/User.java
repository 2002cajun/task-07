package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	public User(){}

	private User(String login){
		this.login=login;
		id=0;
	}

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	@Override
	public String toString() {
		return  login;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		User user = (User) o;
		return Objects.equals(getLogin(), user.getLogin());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLogin());
	}
}
